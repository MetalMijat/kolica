FROM python:3.7.2
LABEL maintainer "Dusan Mijatovic <dusan.mijatovic92@gmail.com>"
RUN apt-get update
RUN mkdir /app
WORKDIR /app
COPY . /app
RUN pip install --no-cache-dir -r requirements.txt
ENV FLASK_ENV="flask"
EXPOSE 5000
