import asyncio, asyncpg, bcrypt
from database import Database


class Account:
    loop = asyncio.get_event_loop()

    @classmethod
    async def find_by_username(cls, username):
        database_parameters = Database.get_database_parameters()

        connection = await asyncpg.connect(
            user=database_parameters['user'],
            password=database_parameters['password'],
            database=database_parameters['database'],
            host=database_parameters['host'],
            port=database_parameters['port']
        )

        result = await connection.fetchrow('''SELECT * FROM account WHERE username=$1''', username)

        await connection.close()

        return result

    @classmethod
    def check_password_validity(cls, password, hashed_password):
        return bcrypt.checkpw(password.encode("UTF8"), hashed_password)
