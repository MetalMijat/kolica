import asyncio
import asyncpg


class Log:
    def __init__(self, log_id, date_time, node_id, beacon_id, rssi_level, beacon_name, beacon_manufacturer):
        self.log_id = log_id
        self.date_time = date_time
        self.node_id = node_id
        self.beacon_id = beacon_id
        self.rssi_level = rssi_level
        self.beacon_name = beacon_name
        self.beacon_manufacturer = beacon_manufacturer

    @classmethod
    async def find_by_id(cls, log_id):
        connection = await asyncpg.connect(user='postgres', password='metal1992', database='kolica', host='127.0.0.1')
        result = await connection.fetchrow('''SELECT * FROM log WHERE id=$1''', log_id)

        await connection.close()

        return result

    @classmethod
    async def find_by_beacon_name(cls, beacon_name):
        connection = await asyncpg.connect(user='postgres', password='metal1992', database='kolica', host='127.0.0.1')
        result = await connection.fetch('''SELECT * FROM log WHERE beacon_name=$1''', beacon_name)

        await connection.close()

        return result


loop = asyncio.get_event_loop()
print(loop.run_until_complete(Log.find_by_id(1)))
print(loop.run_until_complete(Log.find_by_beacon_name('Treci')))
