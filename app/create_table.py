import asyncio, asyncpg, config
from database import Database
import bcrypt


async def create_table():
    database_parameters = Database.get_database_parameters()

    connection = await asyncpg.connect(
                                       user=database_parameters['user'],
                                       password=database_parameters['password'],
                                       database=database_parameters['database'],
                                       host=database_parameters['host'],
                                       port=database_parameters['port']
                                       )

    postgres_uuid_module = '''CREATE EXTENSION IF NOT EXISTS "uuid-ossp";'''

    await connection.execute(postgres_uuid_module)

    create_log_table = '''CREATE TABLE IF NOT EXISTS log(
                                              id uuid DEFAULT uuid_generate_v1 () PRIMARY KEY,
                                              node_id varchar,
                                              data_id varchar,
                                              local_timestamp timestamp,
                                              location_x integer,
                                              location_y integer,
                                              location_z integer,
                                              beacon_id bigint,
                                              beacon_address varchar,
                                              beacon_local_name varchar,
                                              beacon_tx_power_level integer,
                                              rssi real, 
                                              beacon_type varchar, 
                                              ibeacon_uuid varchar,
                                              ibeacon_major integer,
                                              ibeacon_minor integer,
                                              ibeacon_tx_power integer,
                                              beacon_last_seen bigint
                                              )'''

    await connection.execute(create_log_table)

    create_account_table = '''CREATE TABLE IF NOT EXISTS account(id uuid DEFAULT uuid_generate_v1 () PRIMARY KEY, 
                                                           username varchar,
                                                           password bytea
                                                          )'''

    await connection.execute(create_account_table)

    hashed_password = bcrypt.hashpw(config.LOG_ACCOUNT['password'].encode("UTF8"), bcrypt.gensalt())

    create_log_account = '''INSERT INTO account(username, password) VALUES ($1, $2)'''

    await connection.execute(create_log_account, config.LOG_ACCOUNT['username'], hashed_password)

    await connection.close()

asyncio.get_event_loop().run_until_complete(create_table())
