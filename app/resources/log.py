import asyncio, asyncpg
from flask import request
from flask_restful import Resource
from flask_jwt_extended import jwt_required
from database import Database
from datetime import datetime


class Log(Resource):
    loop = asyncio.get_event_loop()

    @classmethod
    async def insert_array_of_logs(cls, logs):
        database_parameters = Database.get_database_parameters()

        connection = await asyncpg.connect(
                                           user=database_parameters['user'],
                                           password=database_parameters['password'],
                                           database=database_parameters['database'],
                                           host=database_parameters['host'],
                                           port=database_parameters['port']
                                           )

        query = '''INSERT INTO log (
                                    node_id,
                                    data_id,
                                    local_timestamp, 
                                    location_x, 
                                    location_y, 
                                    location_z, 
                                    beacon_id,
                                    beacon_address,
                                    beacon_local_name, 
                                    beacon_tx_power_level, 
                                    rssi, 
                                    beacon_type,
                                    ibeacon_uuid, 
                                    ibeacon_major, 
                                    ibeacon_minor, 
                                    ibeacon_tx_power, 
                                    beacon_last_seen
                                    ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17)'''
        if isinstance(logs, list):
            for log in logs:
                await connection.execute(
                                         query,
                                         log['id'],
                                         log['dataId'],
                                         datetime.strptime(log['localTimestamp'], '%Y-%m-%d %H:%M:%S'),
                                         log['locationX'],
                                         log['locationY'],
                                         log['locationZ'],
                                         log['beaconData']['id'],
                                         log['beaconData']['address'],
                                         log['beaconData']['localName'],
                                         log['beaconData']['txPowerLevel'],
                                         log['beaconData']['rssi'],
                                         log['beaconData']['beaconType'],
                                         log['beaconData']['iBeacon']['uuid'],
                                         log['beaconData']['iBeacon']['major'],
                                         log['beaconData']['iBeacon']['minor'],
                                         log['beaconData']['iBeacon']['txPower'],
                                         log['beaconData']['lastSeen']
                                         )
        else:
            await connection.execute(
                                     query,
                                     logs['id'],
                                     logs['dataId'],
                                     datetime.strptime(logs['localTimestamp'], '%Y-%m-%d %H:%M:%S'),
                                     logs['locationX'],
                                     logs['locationY'],
                                     logs['locationZ'],
                                     logs['beaconData']['id'],
                                     logs['beaconData']['address'],
                                     logs['beaconData']['localName'],
                                     logs['beaconData']['txPowerLevel'],
                                     logs['beaconData']['rssi'],
                                     logs['beaconData']['beaconType'],
                                     logs['beaconData']['iBeacon']['uuid'],
                                     logs['beaconData']['iBeacon']['major'],
                                     logs['beaconData']['iBeacon']['minor'],
                                     logs['beaconData']['iBeacon']['txPower'],
                                     logs['beaconData']['lastSeen']
                                     )

        await connection.close()

    @jwt_required
    def post(self):
        logs = request.get_json()

        try:
            self.loop.run_until_complete(self.insert_array_of_logs(logs))
        except:
            return {'message': 'An error occurred while inserting data.'}, 500

        return {'message': 'Data writen successfully'}, 201
