from models.account import Account


def authenticate(username, password):
    account = Account.loop.run_until_complete(Account.find_by_username(username))

    if account and Account.check_password_validity(password, account['password']):
        return True
    return False
