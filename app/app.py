import json, config, os, zipfile
from flask import Flask, request, jsonify, flash, redirect, url_for, send_from_directory
from flask_restful import Api
from flask_mqtt import Mqtt
from flask_jwt_extended import JWTManager, create_access_token
from werkzeug.utils import secure_filename
from resources.log import Log
from security import authenticate

UPLOAD_FOLDER = 'uploads'
ALLOWED_EXTENSIONS = ('json', 'zip',)

app = Flask(__name__)

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['SECRET_KEY'] = 'super secret key'

api = Api(app)

app.config['MQTT_BROKER_URL'] = config.MQTT_CONFIG['broker_url']
app.config['MQTT_BROKER_PORT'] = config.MQTT_CONFIG['broker_port']
app.config['MQTT_USERNAME'] = config.MQTT_CONFIG['username']
app.config['MQTT_PASSWORD'] = config.MQTT_CONFIG['password']
app.config['MQTT_KEEPALIVE'] = config.MQTT_CONFIG['keepalive']
app.config['MQTT_TLS_ENABLED'] = False  # set TLS to disabled for testing purposes

mqtt = Mqtt(app)

app.config['JWT_SECRET_KEY'] = config.JWT_SECRET_KEY
jwt = JWTManager(app)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def is_zip_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() == 'zip'


def change_file_extension_to_json(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[0] + '.json'


@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            folder_path = os.path.join(os.path.dirname(__file__), app.config['UPLOAD_FOLDER'])
            file_path = os.path.join(folder_path, filename)
            file.save(file_path)

            if is_zip_file(filename):
                zip_file = zipfile.ZipFile(file_path, 'r')
                zip_file.extractall(folder_path)
                zip_file.close()

                filename = change_file_extension_to_json(filename)
                file_path = os.path.join(folder_path, filename)

            first_character = open(file_path).readline()[0]

            if first_character == '[':
                logs = json.load(open(file_path, 'r'))
            else:
                logs = []
                with open(file_path) as json_file:
                    for line in json_file:
                        try:
                            log = json.loads(line)
                        except:
                            continue

                        logs.append(log)

            try:
                Log.loop.run_until_complete(Log.insert_array_of_logs(logs))
            except:
                return jsonify({'message': 'An error occurred while inserting data.'}), 500

            return jsonify({'message': 'Data writen successfully'}), 201
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form method=post enctype=multipart/form-data>
      <input type=file name=file>
      <input type=submit value=Upload>
    </form>
    '''


@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'], filename)


@app.route('/login', methods=['POST'])
def login():
    if not request.is_json:
        return jsonify({"message": "Missing JSON in request"}), 400

    username = request.json.get('username', None)
    password = request.json.get('password', None)

    if not username:
        return jsonify({"message": "Missing username parameter"}), 400
    if not password:
        return jsonify({"message": "Missing password parameter"}), 400

    if not authenticate(username, password):
        return jsonify({"message": "Bad username or password"}), 401

    access_token = create_access_token(identity=username)
    return jsonify(access_token=access_token), 200


@mqtt.on_connect()
def handle_connect(client, userdata, flags, rc):
    mqtt.unsubscribe_all()
    mqtt.subscribe('kolica/log')


@mqtt.on_message()
def handle_mqtt_message(client, userdata, message):
    data = dict(
        topic=message.topic,
        payload=message.payload.decode()
    )

    response_message = "Data written successfully"

    logs = json.loads(data['payload'])
    try:
        Log.loop.run_until_complete(Log.insert_array_of_logs(logs))
    except:
        response_message = "Data not written"

    mqtt.publish('kolica/response', response_message)


@app.route('/check')
def check_if_app_is_running():
    return "We are up and running"


api.add_resource(Log, '/log')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
