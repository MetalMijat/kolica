import os
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())

DATABASE_CONFIG = {
    'host': os.getenv("POSTGRES_HOST"),
    'database': os.getenv("POSTGRES_DB"),
    'user': os.getenv("POSTGRES_USER"),
    'password': os.getenv("POSTGRES_PASSWORD"),
    'port': os.getenv("POSTGRES_PORT")
}

MQTT_CONFIG = {
    'broker_url': os.getenv("MQTT_BROKER_URL"),
    'broker_port': int(os.getenv("MQTT_BROKER_PORT")),
    'username': os.getenv("MQTT_USERNAME"),
    'password': os.getenv("MQTT_PASSWORD"),
    'keepalive': int(os.getenv("MQTT_KEEPALIVE"))
}

LOG_ACCOUNT = {
    'username': os.getenv("LOG_USERNAME"),
    'password': os.getenv("LOG_PASSWORD")
}

JWT_SECRET_KEY = os.getenv("JWT_SECRET_KEY")
