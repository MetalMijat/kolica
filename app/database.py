import config, asyncpg


class Database:
    @classmethod
    def get_database_parameters(cls):
        connection = {
                      'user': config.DATABASE_CONFIG['user'],
                      'password': config.DATABASE_CONFIG['password'],
                      'database': config.DATABASE_CONFIG['database'],
                      'host': config.DATABASE_CONFIG['host'],
                      'port': config.DATABASE_CONFIG['port']
                      }

        return connection
