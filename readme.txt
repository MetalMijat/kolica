Welcome to log cart position backend app

This is short tutorial on how to use app:
1. Create .env file and populate data with parameters

POSTGRES_USER=your_postgres_user
POSTGRES_PASSWORD=your_postgres_password
POSTGRES_DB=your_postgres_database_name
POSTGRES_HOST=postgres
POSTGRES_PORT=5432

If you changed PORT in docker-compose.yml file you need to change it here too in order for app to work

NOTE: If you want to recreate your project in the same directory with new DATABASE credentials you need to delete old postgres-data directory
      rm -rf postgres-data


2. Set Mosquitto MQTT broker security

First in sub-folder mosquitto-conf create mosquitto.conf and set "password_file ./your-file-path/yourfile.txt" - it is the same path
as in docker-compose.yaml second path for password.txt. Enter username:password that you want to use and dont forget to put them in .env file

More than one user can be added in this file, one user and password per line
user1:password1
user2:password2

Now we need do encrypt passwords. 
Install mosquitto on local machine: brew install mosquitto - on a Mac, serach for other OS-es
Start it: brew services start mosquitto
Now run command: mosquitto_passwd -U path-to-password-file/passwordfile.txt
Stop it: brew services stop mosquitto

NOTE: This must be done before starting docker-compose,


3. MQTT Broker(Server in new versions) is created in one container. 

You can use MQTT client app to connect with:
MQTT_BROKER_URL=0.0.0.0
MQTT_BROKER_PORT=1883  
Add username and password that you created in step 2.

Add following lines in .env file
MQTT_BROKER_URL=mosquitto  # use the broker docker container name
MQTT_BROKER_PORT=1883  # default port for non-tls connection
MQTT_USERNAME=<your-username>  # set the username here if you need authentication for the broker
MQTT_PASSWORD=<your-passowrd>  # set the password here if the broker demands authentication
MQTT_KEEPALIVE= 30  # set the time interval for sending a ping to the broker to 5 seconds
MQTT_TLS_ENABLED=False  # set TLS to disabled for testing purposes

Topics for logging are 'kolica/log' - to write log, 'kolica/test' to see responses from loging.

You can send same json format as with POST request and it will be written in database (Check step 6.)


4. Add secret key for JWT token in .env file

JWT_SECRET_KEY=your-secret-key


5. To use 0.0.0.0:5000/log you need to use username and password that are defined in .env file

LOG_USERNAME=your-username
LOG_PASSWORD=your-password

and you need to POST on 0.0.0.0:5000/login with json:

{
	"username": "your-username",
	"password": "your-password"
}

After that add Bearer jwt_token in Authorization header of your requests


6. From directory run following commands to create and run this project

docker-compose build
docker-compose up

NOTE: To check if app is running use http://0.0.0.0:5000/check, dont forget to login first (check 4.)


7. Run create table script, it will create log table in your database
   docker-compose exec app python app/create_table.py

NOTE Your docker containers must be running


8. To log list of logs you use route /log

Send POST request to http://0.0.0.0:5000/log with json format

[
	{
	"id": "4asdad321",
	"dataId": "19388467-f00c-4b3a-81ee-8c663bb6ea79",
	"localTimestamp": "2019-03-02 18:52:10",
	"locationX": 5,
	"locationY": 8,
	"locationZ": 7,
	"beaconData": {
		"id": 1551552730551,
		"address": "c7:53:3d:11:d9:44",
		"localName": "C-Digit",
		"txPowerLevel": 0,
		"rssi": -62,
		"beaconType": "iBeacon",
		"iBeacon": {
			"uuid": "FFFE2D121E4B0FA4994ECEB531F40545",
			"major": 4660,
			"minor": 22136,
			"txPower": 49
		},
		"lastSeen": 1551552730551
	}
},
{
	"id": "4asdad321",
	"dataId": "19388467-f00c-4b3a-81ee-8c663bb6ea79",
	"localTimestamp": "2019-03-02 18:52:10",
	"locationX": 5,
	"locationY": 8,
	"locationZ": 7,
	"beaconData": {
		"id": 1551552730551,
		"address": "c7:53:3d:11:d9:44",
		"localName": "C-Digit",
		"txPowerLevel": 0,
		"rssi": -62,
		"beaconType": "iBeacon",
		"iBeacon": {
			"uuid": "FFFE2D121E4B0FA4994ECEB531F40545",
			"major": 4660,
			"minor": 22136,
			"txPower": 49
		},
		"lastSeen": 1551552730551
	}
}
]

You can sand as many logs in array as you want. We use async database writing so it can handle millions of rows in seconds.
NOTE: Can be done from MQTT client 'kolica/response' and send same JSON format


9. Files can be uploaded and written to database like this:
    a. With URL: http://35.204.40.27:5000/upload 
    b. Or with curl: $ curl -i -X POST -H "Content-Type: multipart/form-data" -F "file=@/path/to/file/file.zip" http://35.204.40.27:5000/upload

    Files can be of type .zip or .tar.gz it's important that suffix is .zip. Also it is possible to upload files of type .json with suffix .json

    NOTE: It's important that json file that is in the archive has the same name as archive test.json in test.zip
  
  Format of .json file can be:
    a. Like above mentioned valid JSON array,
    b. Valid JSON array minified - valid JSON in one line,

    [{"id":"ec0f0cb8-7fe6-440b-98d7-5f70462c4dbd","dataId":"Peti","localTimestamp":"2014-11-22 01:03:35","locationX":2,"locationY":6,"locationZ":4,"beaconData":{"id":184972322670,"address":"Treci","localName":"Njegov","txPowerLevel":3,"rssi":138.62,"beaconType":"iBeacon","iBeacon":{"uuid":"a25b3fed-2f1d-42b7-8372-47344d9bbcd9","major":19601,"minor":16536,"txPower":294},"lastSeen":182748164456}},{"id":"41436d8b-67a5-4c6a-9ec5-189f33b5a26e","dataId":"Treci","localTimestamp":"2018-11-08 02:29:19","locationX":7,"locationY":4,"locationZ":9,"beaconData":{"id":182479791715,"address":"Drugi","localName":"OnogTamo","txPowerLevel":1,"rssi":4982.62,"beaconType":"iBeacon","iBeacon":{"uuid":"ee70b513-c6b9-4e51-bba7-2239815e2293","major":13159,"minor":16052,"txPower":426},"lastSeen":122403912426}}]
    
    c. Every JSON object in now line

    {"id":"0000000051f4b157","dataId":"2edfb713-1517-4414-8faf-38ceb1b71f85","localTimestamp":"2019-03-11 17:53:15","locationX":1,"locationY":2,"locationZ":3,"beaconData":{"id":248720425928889,"address":"e2:35:bc:a2:c8:b9","localName":"C-Digit","txPowerLevel":0,"rssi":-67,"beaconType":"iBeacon","iBeacon":{"uuid":"FFFE2D121E4B0FA4994ECEB531F40545","major":4660,"minor":22136,"txPower":49},"lastSeen":1552326787473}}
    {"id":"0000000051f4b157","dataId":"c3723b72-dcd4-4da9-bb6b-0e287e98ef36","localTimestamp":"2019-03-11 17:53:15","locationX":1,"locationY":2,"locationZ":3,"beaconData":{"id":219160320792900,"address":"c7:53:3d:11:d9:44","localName":"C-Digit","txPowerLevel":0,"rssi":-59,"beaconType":"iBeacon","iBeacon":{"uuid":"FFFE2D121E4B0FA4994ECEB531F40545","major":4660,"minor":22136,"txPower":49},"lastSeen":1552326787422}}


10. To connect to database use command:

docker exec -it <your-directory-name>_postgres_1 psql -h postgres -d <database-name> -U <database-user>

after that use <database-password> to connect. All values are the same as defined in your .env file


11. To stop app from running press Ctrl + C, and if you want to delete it firs run:

docker-compose down

and than delete all files.


12. JSON Generator
To use JSON generator for testing, go to url: https://www.json-generator.com/# and use the JavaScript code below:

[
  '{{repeat(10, 14)}}',
  {
    id: '{{guid()}}',
    dataId: '{{random("Prvi", "Drugi",	"Treci", "Cetvrti", "Peti")}}',
    localTimestamp: '{{date(new Date(2014, 0, 1), new Date(), "YYYY-MM-dd hh:mm:ss")}}',
	locationX: '{{integer(1, 10)}}',
    locationY: '{{integer(1, 10)}}',
    locationZ: '{{integer(1, 10)}}',
    beaconData: {
      id: '{{integer(100000000000, 200000000000)}}',
      address: '{{random("Prvi", "Drugi",	"Treci", "Cetvrti", "Peti")}}',
      localName: '{{random("Moj", "Tvoj", "Njegov", "Njen", "OnogTamo")}}',
      txPowerLevel: '{{integer(1, 10)}}',
      rssi: '{{floating(100.01, 6000)}}',
      beaconType: "iBeacon",
      iBeacon: {
        uuid: '{{guid()}}',
        major: '{{integer(10000, 20000)}}',
        minor: '{{integer(10000, 20000)}}',
        txPower: '{{integer(200, 500)}}'
      },
      lastSeen: '{{integer(100000000000, 200000000000)}}'
    }
  }
]
